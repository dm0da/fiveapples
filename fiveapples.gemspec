
require_relative 'lib/version'

D = 'Fiveapples is a fully opensource and standalone OData + openui5 demo app. It runs a
sqlite database, serves it as a Safrano based OData endpoint
and additionally serves the openui5 demo app itself, all in a single ruby process.'.freeze

F = Dir['lib/*.rb'] + Dir['lib/fiveapples.db3'] + Dir['lib/config.ru']
F += Dir['bin/fiveapples'] + Dir['lib/ui5/**/*'] + Dir['lib/ui5_1way/**/*']
F += Dir["lib/openui5-runtime-#{OpenUI5::VERSION}/**/*"]

Gem::Specification.new do |s|
  s.name        = 'fiveapples'
  s.version     = FiveApples::VERSION
  s.summary     = 'Fiveapples is a fully opensource standalone OData + openui5 demo app.'
  s.description = D
  s.authors     = ['oz']
  s.email       = 'dev@aithscel.eu'
  s.files       = F
  s.bindir      = 'bin'
  s.executables << 'fiveapples'
  s.rdoc_options << "--exclude=lib/openui5-runtime*"
  s.required_ruby_version = '>= 2.4.0'
  s.add_dependency 'safrano', '~> 0.4', '>= 0.4.1'
  s.add_dependency 'puma', '~> 6.0'
  s.add_dependency 'sqlite3', '~> 1.3', '>= 1.3.9'
  s.add_development_dependency 'rake', '~> 12.3'
  s.add_development_dependency 'capybara', '~> 3.0'
  s.add_development_dependency 'rubocop', '~> 0.51'
  s.add_development_dependency  'selenium-webdriver', '~> 4.0'
  s.homepage = 'https://gitlab.com/dm0da/fiveapples'
  s.license = 'MIT'
  s.metadata = {
    "bug_tracker_uri" => "https://gitlab.com/dm0da/fiveapples/issues",
    "changelog_uri" => "https://gitlab.com/dm0da/fiveapples/blob/master/CHANGELOG",
    "source_code_uri" => "https://gitlab.com/dm0da/fiveapples/tree/master",
    "wiki_uri" => "https://gitlab.com/dm0da/fiveapples/wikis/home"
  }
end
