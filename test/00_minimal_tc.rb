
require_relative './test_helper_mini.rb'
require 'rest_client'

class MinimalTestCase < TCMini

  def setup
    @rootpage = "http://#{FiveA.host.to_s}:#{FiveA.port}"
  end
  
  def test_test
    assert true
  end

  def test_odata_service
  # Get a record with GET
    response = RestClient.get "#{@rootpage}/odata"
    assert_equal 200, response.code
    if Safrano::VERSION <= '0.4.3'
      assert_equal 'application/atomsvc+xml;charset=utf-8', response.headers[:content_type]
    else
      assert_equal 'application/xml;charset=utf-8', response.headers[:content_type]
    end
  end
  
  def test_odata_service_meta
  # Get a record with GET
    response = RestClient.get "#{@rootpage}/odata/$metadata"
    assert_equal 200, response.code
    assert_equal 'application/xml;charset=utf-8', response.headers[:content_type]
  end  

 def test_odata_service_content
  # Get a record with GET
    response = RestClient.get "#{@rootpage}/odata/cultivar(1)"
    assert_equal 200, response.code
    assert_equal 'application/json;charset=utf-8', response.headers[:content_type]
  end    
  
  def test_ui5_startpage
  # Get a record with GET
    response = RestClient.get "#{@rootpage}/ui5/index.html"
    assert_equal 200, response.code
  end   
  
end