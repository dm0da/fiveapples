
require_relative './test_helper.rb'


class BasicUpdateTestCase < TCBase

  def setup
    @startpage = "http://#{FiveA.host.to_s}:9494/#{FiveA.ui5d}"
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
  
  def teardown
#    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
  
  def test_test
    assert true
  end   
  
  def test_click_navigate_to_cultivar_detail_update
    visit(@startpage)
    @detailpath = "index.html#/cultivar/2"
    e = page.find('span', text: 'Weißer Winterglockenapfel')
    
    assert_path_after_click(@detailpath) {e.click}

    e = page.find_field 'Year'
    current_yr = Cultivar[2].year
    assert_equal current_yr.to_s, e.value
    
# add two years

    e.set(Cultivar[2].year + 2)
    b = page.find_button 'Save changes'
    assert_path_after_click(@detailpath) {b.click}
    
# give some time to reach the backend
    expected_yr = current_yr + 2
#    assert_equal expected_yr, Cultivar[2].year 
    WaitFor::assert_equal(expected_yr) { Cultivar[2].year }
  end
  
  def test_click_navigate_to_breeder_detail_update
   
    @detailpath = "index.html#/breeder/1"
    visit(@startpage)
    
    e = page.find('li', text: 'List of Breeders')
    
    assert_path_after_click('index.html#/breederList') { e.click }
    e = page.find('span', text: 'Taylor')
    
    
    assert_path_after_click(@detailpath) {e.click; page.refresh }
    
  # find by form field name = year works bette here ? 
    fn = page.find_field 'First name'
    ln = page.find_field 'Last name'

    oldfn = fn.value.dup
    oldln = ln.value.dup

# append some data

    #native_fill(fn, '__')
    #native_fill(ln, '__')
    native_fill(fn, "#{oldfn}__")
    native_fill(ln, "#{oldln}__")    
    
    b = page.find_button 'Save changes'
    assert_path_after_click(@detailpath) {b.click}
    
## give some time to reach the backend and check
    WaitFor::assert_equal("#{oldfn}__") { Breeder[1].first_name }
    WaitFor::assert_equal("#{oldln}__") { Breeder[1].last_name }

  end
end