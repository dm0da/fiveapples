
require 'test/unit'
require 'rest_client'
require 'capybara/dsl'
require 'selenium-webdriver'


class Class
  def descendants
    ObjectSpace.each_object(::Class).select {|klass| klass < self }
  end
end


# more work needed for timezone. But until now to get
# at least something and the correct iso formating, we set UTC
ENV['TZ'] = 'UTC'

module WaitFor

def WaitFor.constant_loaded(&proc)
    begin
      Timeout::timeout(4) do
        rx = true
        while rx
          begin
            proc.call
            rx = false 
          rescue NameError => e
            rx=true
            sleep 0.2
          end
        end
      end
    rescue
      raise "Timeout: #{myconst.to_s} not yet loaded/defined"
    end  
  end

  def WaitFor.assert_equal(expected, &current_proc)
    begin
      Timeout::timeout(4) do
        while expected != current_proc.call 
          sleep 0.2
        end
      end
    rescue
      raise "Timeout: #{current_proc} not equal #{expected}"
    end  
  end
end

class TCBase < Test::Unit::TestCase
  include Capybara::DSL
  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end
  # quick and dirty OData client
  module ODataClient
    ROOT = "http://#{FiveA.host.to_s}:9494/odata"
    def self.with_response(path)
      yield RestClient.get  "#{ROOT}/#{path}"
    end
    
    def self.with_parsed(resp)
      yield JSON.parse(resp.body)['d']
    end
    def self.with_parsed_coll(resp)
      yield JSON.parse(resp.body)['d']['results']
    end
  end

  # Workaround chrome driver bug and/or too much client-side javascript ?
  # field.set and even field.send_keys frequently end up eating part of the sent text
  # and it results in follow-up errors.
  # Workaround by sending each char, waiting a bit +checking + timeout+ try resending once
  
  # Warning this is actually "append" (ie. fill when it's empty on start)
  def native_fill(field, text)
    return  field.set(text)
    
    
    expt = field.value.dup
    text.split('').each { |c| 
      expt << c
      field.native.send_keys(c) 
      begin
        Timeout::timeout(1) do
          while field.value  != expt
            sleep 0.2
          end
        end
      rescue Timeout::Error
# retry as it seems that chrome driver  sometimes eats the first char      
        field.native.send_keys(c)
        begin
        Timeout::timeout(1) do
          while field.value  != expt
            sleep 0.2
          end
        end
        rescue Timeout::Error
          require 'pry'
          binding.pry
        end
      end
      }
  end
  
  def assert_path_after_click(path, &click_proc)
    begin
      click_proc.call
      Timeout::timeout(5) do
        while "#{@startpage}/#{path}" != page.current_url
          sleep 0.5
        end
      end
    rescue Timeout::Error
# retry as it seems that chrome driver  sometimes eats the click?
      click_proc.call
      begin
        Timeout::timeout(5) do
          while "#{@startpage}/#{path}" != page.current_url
            sleep 0.5
          end
        end
      rescue Timeout::Error
        assert false, "Timeout: page.current_url != #{path}"
      end
    end
  
  end
  
end



  Capybara.register_driver :remchrome do |app|
    browser_options = ::Selenium::WebDriver::Chrome::Options.new
    browser_options.add_argument("headless")
    browser_options.add_argument("no-sandbox")
    browser_options.add_argument("disable-dev-shm-usage") 

    url =  ENV['selenium_remote_url']

    
    Capybara::Selenium::Driver.new app,
                                   browser: :remote,
                                   # desired_capabilities: :chrome,
                                   options: browser_options,
                                   url: url
  end

  Capybara.javascript_driver = Capybara.default_driver = :remchrome
  

  Capybara.default_max_wait_time = 5
