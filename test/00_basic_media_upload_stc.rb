
require_relative './test_helper.rb'

require 'timeout'
require 'json'

class BasicMediaUploadTestCase < TCBase

  def setup
    @startpage = "http://#{FiveA.host.to_s}:9494/#{FiveA.ui5d}"
    Capybara.reset_sessions!
    Capybara.use_default_driver
    
    @site = "http://#{FiveA.host.to_s}:9494"

  end
  
  def teardown
#    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
  
  def test_test
    assert true
  end
  
  def test_cultivar_upload_one_image
    WaitFor.constant_loaded{Cultivar}
    cult = Cultivar.find( :name => 'Topaz' )
    
    @detailpath = "index.html#/cultivar/#{cult.id}"
    visit(@startpage)
    e = page.find('span', text: 'Topaz')
    
    assert_path_after_click(@detailpath) {e.click}
    
    attach_file 'myFileUpload', '../test/uploadfiles/Topaz_Apfel.jpg' 
    u = page.find_button 'Upload'
    assert_path_after_click(@detailpath) {u.click}

# toasted ?
    assert page.has_content? 'File Uploaded'
    # after upload the file input field shall be cleared
    page.find_field 'myFileUpload', with: ''
    
# get the image media entity uri by querying odata collection    
    ODataClient.with_response("cultivar(#{cult.id})?$expand=photo") do |resp|
      assert_equal 200, resp.code
      assert_equal 'application/json;charset=utf-8', resp.headers[:content_type]
      @imgsrc = ODataClient.with_parsed(resp) do |jresd|
         jresd['photo']['results'].first['__metadata']['media_src']
      end
## image with src http://localhost:9494/odata/Photo(xx)/$value
      assert page.has_xpath? %Q(.//img[@src="#{@imgsrc}"])

    end

  
## go to another Cultivar detail page
  
    cult = Cultivar.find( :name => 'Rubin' )
    
    @detailpath = "index.html#/cultivar/#{cult.id}"
    visit(@startpage)
    e = page.find('span', text: 'Rubin')
    
    assert_path_after_click(@detailpath) {e.click}  
  
## and check that it does not have this image  
    assert page.has_no_xpath? %Q(.//img[@src="#{@imgsrc}"])
  end

  
  def test_cultivar_upload_two_images
    WaitFor.constant_loaded{Cultivar}
    cult = Cultivar.find( :name => 'Gloster' )
    
    @detailpath = "index.html#/cultivar/#{cult.id}"
    visit(@startpage)
    e = page.find('span', text: 'Gloster')
    
    assert_path_after_click(@detailpath) {e.click}
    
    u = page.find_button 'Upload'
    
    attach_file 'myFileUpload', '../test/uploadfiles/533px-Glosterapple.jpeg' 
    assert_path_after_click(@detailpath) {u.click}

# toasted ?
    assert page.has_content? 'File Uploaded'
    
    # after upload the file input field shall be cleared
    page.find_field 'myFileUpload', with: ''
    
# upload a second one    
     attach_file 'myFileUpload', '../test/uploadfiles/Malus_Gloster_4558.jpg' 
     assert_path_after_click(@detailpath) {u.click}
   
# get the image media entity uri by querying odata    

    
   ODataClient.with_response("cultivar(#{cult.id})?$expand=photo") do |resp|
      assert_equal 200, resp.code
      assert_equal 'application/json;charset=utf-8', resp.headers[:content_type]
      ODataClient.with_parsed(resp) do |jresd|
      jresd['photo']['results'].each{|photo|
        @imgsrc = photo['__metadata']['media_src']
        assert page.has_xpath? %Q(.//img[@src="#{@imgsrc}"])
      }
      
## go to another Cultivar detail page
  
    cult = Cultivar.find( :name => 'Rubin' )
    
    @detailpath = "index.html#/cultivar/#{cult.id}"
    visit(@startpage)
    e = page.find('span', text: 'Rubin')
    
    assert_path_after_click(@detailpath) {e.click}  
    jresd['photo']['results'].each{|photo|
        @imgsrc = photo['__metadata']['media_src']
        assert page.has_no_xpath? %Q(.//img[@src="#{@imgsrc}"])
      }
         
      end
    end    
  end

  def test_breeder_upload_avatar
    WaitFor.constant_loaded{Breeder}
    brd = Breeder.find( :last_name => 'Appius' )
    
    @detailpath = "index.html#/breeder/#{brd.id}"
    visit(@startpage)
    
    e = page.find('li', text: 'List of Breeders')
    assert_path_after_click('index.html#/breederList') { e.click; page.refresh }
    
    e = page.find('span', text: 'Appius')
    
    assert_path_after_click(@detailpath) {e.click; page.refresh }
    
    attach_file 'myAvatarUpload', '../test/uploadfiles/appius-claudius-caecus.jpeg' 
    u = page.find_button 'Upload Avatar'
    assert_path_after_click(@detailpath) {u.click}

# toasted ?
    assert page.has_content? 'File Uploaded'
    # after upload the file input field shall be cleared
    page.find_field 'myAvatarUpload', with: ''
    
#    detect the avatar image 
# Note currently we are testing the media source value, which is 
# a bit implementation dependant.
# Ideally (TODO) we shall download the data behind the media uri, 
# and test that it is same as the uploaded . This would be fully implementation
# independant.

    visit "#{@startpage}/#{@detailpath}"
#     background-image:url('http://localhost:9494/odata/Photo(10)/$value')
# get the image media entity uri by querying navigation links   
   ODataClient.with_response("breeder(#{brd.id})?$expand=avatar") do |resp|
      assert_equal 200, resp.code
      assert_equal 'application/json;charset=utf-8', resp.headers[:content_type]
      @imgsrc = ODataClient.with_parsed(resp) do |jresd|
         jresd['avatar']['__metadata']['media_src']
      end
      @bgstyle = "background-image:url('#{@imgsrc}')"
    
      apanel = page.find(:xpath, './/div[@id="__panel0-content"]')
      assert apanel.find(:xpath, '//span[@role="img"]',
                     :style =>  @bgstyle, 
                     :visible => :all)
    end 
    
    
### go to another Breeder detail page
  
    other = Breeder.find( :last_name => 'Tupy' )
    
    @detailpath = "index.html#/breeder/#{other.id}"
    visit "#{@startpage}/#{@detailpath}"
   
    page.find_field 'last_name', with: 'Tupy'
    apanel = page.find(:xpath, './/div[@id="__panel0-content"]')
    
### Avatar should exist...
    assert apanel.has_xpath?('//span[@role="img"]', :visible => :all)    

# but not with the previously uploaded image    
    assert apanel.has_no_xpath?('//span[@role="img"]', 
                               :style =>  @bgstyle, 
                               :visible => :all)
    
  end

  def test_breeder_update_avatar
    WaitFor.constant_loaded{Breeder}
    last_name = 'Bramley'
    brd = Breeder.find( :last_name => last_name )
  
    @detailpath = "index.html#/breeder/#{brd.id}"
    visit(@startpage)
    
    e = page.find('li', text: 'List of Breeders')
    assert_path_after_click('index.html#/breederList') { e.click }
    
    e = page.find('span', text: last_name)
    
    assert_path_after_click(@detailpath) {e.click}
    
    attach_file 'myAvatarUpload', '../test/uploadfiles/Matthew-Bramley.png' 
    u = page.find_button 'Upload Avatar'
    assert_path_after_click(@detailpath) {u.click}

# toasted ?
    assert page.has_content? 'File Uploaded'
    # after upload the file input field shall be cleared
    page.find_field 'myAvatarUpload', with: ''
    
#    detect the avatar image 
# Note currently we are testing the media source value, which is 
# a bit implementation dependant.
# Ideally (TODO) we shall download the data behind the media uri, 
# and test that it is same as the uploaded . This would be fully implementation
# independant.

    visit "#{@startpage}/#{@detailpath}"
#     background-image:url('http://localhost:9494/odata/Photo(10)/$value')
# get the image media entity uri by querying navigation links   
   ODataClient.with_response("breeder(#{brd.id})?$expand=avatar") do |resp|
      assert_equal 200, resp.code
      assert_equal 'application/json;charset=utf-8', resp.headers[:content_type]
      @imgsrc = ODataClient.with_parsed(resp) do |jresd|
         jresd['avatar']['__metadata']['media_src']
      end
      @bgstyle = "background-image:url('#{@imgsrc}')"
    
      apanel = page.find(:xpath, './/div[@id="__panel0-content"]')
      assert apanel.find(:xpath, '//span[@role="img"]',
                     :style =>  @bgstyle, 
                     :visible => :all)
    end 

    first_imgsrc = @imgsrc.dup

# second upload  --> Update !
    attach_file 'myAvatarUpload', '../test/uploadfiles/_86297895_bramley_comp.jpg' 
    u = page.find_button 'Upload Avatar'
    assert_path_after_click(@detailpath) {u.click}

# toasted ?
    assert page.has_content? 'File Uploaded'
    # after upload the file input field shall be cleared
    page.find_field 'myAvatarUpload', with: ''
    
#    detect the avatar image 
# Note currently we are testing the media source value, which is 
# a bit implementation dependant.
# Ideally (TODO) we shall download the data behind the media uri, 
# and test that it is same as the uploaded . This would be fully implementation
# independant.
    
    visit "#{@startpage}/#{@detailpath}"
#     background-image:url('http://localhost:9494/odata/Photo(10)/$value')
# get the image media entity uri by querying navigation links   
   ODataClient.with_response("breeder(#{brd.id})?$expand=avatar") do |resp|
      assert_equal 200, resp.code
      assert_equal 'application/json;charset=utf-8', resp.headers[:content_type]
      @imgsrc = ODataClient.with_parsed(resp) do |jresd|
         jresd['avatar']['__metadata']['media_src']
      end
      
# check that after a second upload the media_src is different
      assert ( first_imgsrc != @imgsrc ), "image source do not differ, first = #{first_imgsrc}, current=#{@imgsrc}"
      
      @bgstyle = "background-image:url('#{@imgsrc}')"
    
      apanel = page.find(:xpath, './/div[@id="__panel0-content"]')
      assert apanel.find(:xpath, '//span[@role="img"]',
                     :style =>  @bgstyle, 
                     :visible => :all)
    end 
       
  end  
  
  
end