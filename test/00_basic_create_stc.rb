
require_relative './test_helper.rb'

require 'timeout'

class BasicCreateTestCase < TCBase

  def setup
    @startpage = "http://#{FiveA.host.to_s}:9494/#{FiveA.ui5d}"
    Capybara.reset_sessions!
    Capybara.use_default_driver
    @new_entities = []
  end
  
  def teardown
#    Capybara.reset_sessions!
    Capybara.use_default_driver
# cleanup to avoid having to scroll down in lists due to 
# too much test-data     
    nb_expected = Cultivar.count + Breeder.count - @new_entities.size
    @new_entities.each{|e| e.delete }
    WaitFor::assert_equal(nb_expected) { Cultivar.count + Breeder.count }
  end
  
  def test_test
    assert true
  end
  
  def test_click_cultivar_create
    visit(@startpage)
    asserts_click_cultivar_create_one('ycyvxcvx', 1232)
  end
  
  # test that it works multiple times (did not work in TwoWay binding)
  def test_click_cultivar_create_twice
    visit(@startpage)
    asserts_click_cultivar_create_one('asdfghvyxc', 6423)
    
    visit(@startpage)
    asserts_click_cultivar_create_one('qwerrtfs', 1342)
  end
  
  # re-useable testcase
  def asserts_click_cultivar_create_one(newname, newyear)
    
    
    b = page.find_button 'Create'
    
    assert_path_after_click('index.html#/cultivar_create') { b.click }
    
    WaitFor.constant_loaded{Cultivar}
    nb_cultivars = Cultivar.count
# fill in some data

    # find by Label = Name
    # page.fill_in 'Name', with: newname
    
    # find by form field name = year works bette here ? 
    e = page.find_field 'Year'
    n = page.find_field 'Name'
#    e.set(newyear.to_s)
#    n.set(newname)
    native_fill(e, newyear.to_s)
    native_fill(n, newname)
    
    c = page.find_button 'Create'
    c.click

# normally after creation, page is navigated to new cultivar detail page
# we can get the new id from there !
    h1 = page.find('h1')
    assert h1
    
    e = h1.find(:xpath, './/span',
                        id: /titleText-inner\z/, 
                        text: /\A\d+\s:\s#{newname}\z/ )
    
    assert e.text  =~ /\A(\d+)\s:\s#{newname}\z/
    new_id = Regexp.last_match(1).to_i
    
    assert (new_id > 0)
# DB checks are dangerous du to threads... --> failing randomly on gitlab CI :-(
    e = page.find_field 'Year'

    WaitFor::assert_equal(newyear.to_s) {e.value}
    
    # check that we have one more
    nb_expected = nb_cultivars + 1
    
#    assert_collection_count(nb_expected, Cultivar)

    WaitFor::assert_equal(nb_expected) { Cultivar.count }

    Cultivar[new_id].refresh
    @new_entities << Cultivar[new_id]
    
# check the new cultivar on DB. We get the new-id from the current page text !
    WaitFor::assert_equal(newname){ Cultivar[new_id].name }
    WaitFor::assert_equal(newyear){ Cultivar[new_id].year.to_i }
    WaitFor::assert_equal(nil){ Cultivar[new_id].breeder_id }
    WaitFor::assert_equal(nil){ Cultivar[new_id].inst_id }
    
  end
  
  def test_click_breeder_create
    visit(@startpage)
    asserts_click_breeder_create_one
  end
  
  def test_click_breeder_create_twice
    visit(@startpage)
    asserts_click_breeder_create_one
    visit(@startpage)
    asserts_click_breeder_create_one    
  end
  
  def asserts_click_breeder_create_one
    
    
    e = page.find('li', text: 'List of Breeders')
    assert_path_after_click('index.html#/breederList') { e.click }
    
    b = page.find_button 'Create record'
    
    # find by Label = Name
    # page.fill_in 'Name', with: newname
    assert_path_after_click('index.html#/breeder_create') { b.click }
    
    nb_breeder= Breeder.count
    
    # find by form field name = year works bette here ? 
    fn = page.find_field 'First name'
    ln = page.find_field 'Last name'

# fill in some data
    newfname = 'Pierre'
    newlname = 'Le Lectier'

#    fn.set(newfname)
#    fn.send_keys(newfname)
     native_fill(fn, newfname)
#    ln.set(newlname)
#    ln.send_keys(newlname)
    native_fill(ln, newlname)
    
    c = page.find_button 'Create'
    c.click
    
    
# normally after creation, page is navigated to new breeder detail page
# we can get the new id from there !
    h1 = page.find('h1')
    assert h1
    
    e = h1.find(:xpath, './/span',
                        id: /titleText-inner\z/, 
                        text: /\A\s*id\s*:\s*\d+\z/ )
    
    assert e.text  =~ /\A\s*id\s*:\s*(\d+)\z/
    new_id = Regexp.last_match(1).to_i
    
    assert (new_id > 0)
# DB checks are dangerous du to threads... --> failing randomly on gitlab CI :-(

  
# check that we have one more
    nb_expected = nb_breeder + 1
    WaitFor::assert_equal(nb_expected) { Breeder.count }
    
    Breeder[new_id].refresh
    @new_entities << Breeder[new_id]
    
# check the new cultivar on DB. We get the new-id from the current page text !
    assert_equal newfname, Breeder[new_id].first_name
    assert_equal newlname, Breeder[new_id].last_name
   
    
  end

  
end