
require_relative './test_helper.rb'


class BasicNavigationTestCase < TCBase

  def setup
    @startpage = "http://#{FiveA.host.to_s}:9494/#{FiveA.ui5d}"
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
  def teardown
#    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
  
  def test_test
    assert true
  end
  
  def test_firstpage_contents
    visit( @startpage )
    assert_equal 'Fiveapples demo app', page.title
    
    assert page.has_text? 'Gloster'
    assert page.has_text? 'Yellow Bellflower'
    assert page.has_text? 'Weißer Winterglockenapfel'
    assert page.has_table?

  end
  
  def test_click_navigate_to_detail
    WaitFor.constant_loaded{Cultivar}
    cult = Cultivar.find( :name => "Weißer Winterglockenapfel" )
  
    visit(@startpage)
    e = page.find('span', text: 'Weißer Winterglockenapfel')
    
    assert_path_after_click("index.html#/cultivar/#{cult.id}") {e.click}
    
  end

  def test_cultivar_breeder_avatar_initials
    WaitFor.constant_loaded{Cultivar}
    cult = Cultivar.find( :name => "Cripps Pink" )
    visit(@startpage)
    e = page.find('span', text: "Cripps Pink")
    
    assert_path_after_click("index.html#/cultivar/#{cult.id}") {e.click}
    
    # check avatar as initials --> John Cripps
    assert page.find('span', class: 'sapFAvatarInitialsHolder', 
                  text: 'JC',
# test doesnt work without visible all. Why ? 
# the element is actually visible ?                  
                  :visible => :all)
  end
  

  def test_menu_click
    visit(@startpage)
    e = page.find('li', text: 'Parentage list')
    
    assert_path_after_click("index.html#/parentageList") {e.click}
    assert page.has_text? 'Gloster'
    assert page.has_text? 'Yellow Bellflower'
    assert page.has_text? 'seedling'
    assert page.has_text? 'mother'
  end
  
  def test_parentage_list_navto_detail
    visit(@startpage)
    e = page.find('li', text: 'Parentage list')
    assert_path_after_click("index.html#/parentageList") {e.click}
    
    e = page.find('span', text: 'Yellow Bellflower')   
    assert_path_after_click("index.html#/parentage/3/3/4") {e.click} 
    
    assert page.has_text? 'Red Delicious'
    assert page.has_text? 'Yellow Bellflower'
    assert page.has_text? 'seedling'
  end  
  
  def test_breeder_list_navto_detail
    visit(@startpage)
    e = page.find('li', text: 'List of Breeders')
    assert_path_after_click("index.html#/breederList") {e.click}
    
    e = page.find('span', text: 'Baron')
    assert_path_after_click("index.html#/breeder/5") {e.click}
    
    e = page.find_field 'First name'
    assert_equal 'Pierre', e.value
    e = page.find_field 'Last name'
    assert_equal 'Baron', e.value
    
    # Cultivars sub-list
    assert page.has_text? 'Belle fille de Salins'
    
    # check avatar as initials
    assert page.find('span', class: 'sapFAvatarInitialsHolder', 
                  text: 'PB',
# test doesnt work without visible all. Why ? 
# the element is actually visible ?                  
                  :visible => :all)
                      
  end  
  
  
  
end