
require 'test/unit'

class Class
  def descendants
    ObjectSpace.each_object(::Class).select {|klass| klass < self }
  end
end


# more work needed for timezone. But until now to get
# at least something and the correct iso formating, we set UTC
ENV['TZ'] = 'UTC'


class TCMini < Test::Unit::TestCase
  #test suite based on all TC subclasses
  def self.subsuites
    suites = Test::Unit::TestSuite.new
    self.descendants.each{|tc| suites << tc.suite }
    suites
  end

end
