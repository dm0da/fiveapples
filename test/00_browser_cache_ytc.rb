
require 'timeout'
require 'rest-client'
require_relative '../lib/fa_utils'
require_relative './test_helper.rb'


class TCStandalone < TCBase

  def setup
    @host = if ( ENV['selenium_remote_url'] )
        FiveApples.test_host_ip()
      else
        'localhost'
      end
    @rootpage = "http://#{@host}:9494"
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
  def teardown
    Capybara.use_default_driver
  end
  
  def startserver(optstring='--test')
#    @pid = spawn("bin/fiveapples #{optstring}")
    @pid = spawn("bin/fiveapples #{optstring}", [:out, :err]=>"/dev/null")
    
    Process.detach @pid

# Wait until the server is started and responds with something
    Timeout::timeout(4) do
      begin
        response = RestClient.get "#{@rootpage}/odata"
      rescue 
        response = nil
      end
      until response
        sleep 0.2
        begin
          response = RestClient.get "#{@rootpage}/odata"
        rescue 
          response = nil
        end
      end
      puts "Fiveapples Proc #{@pid} Started with : #{optstring}" if response
    end
    
  end
  
  def stopserver
    if @pid 
      Process.kill('TERM', @pid) 
      # Wait until the server is started and responds with 200
      Timeout::timeout(4) do
        begin
          response = RestClient.get "#{@rootpage}/odata"
        rescue 
          response = nil
        end
        while response
          sleep 0.2
          begin
            response = RestClient.get("#{@rootpage}/odata")
          rescue 
            response = nil
          end
        end
        puts "Fiveapples Proc #{@pid} Stopped !" 
      end
    end
  end
    
  def test_firstpage_contents
    startserver
    begin

      visit( "#{@rootpage}/ui5/index.html" )
      assert_equal 'Fiveapples demo app', page.title
      
      assert page.has_text? 'Gloster'
      assert page.has_text? 'Yellow Bellflower'
      assert page.has_text? 'Weißer Winterglockenapfel'
      assert page.has_table?
    ensure
      stopserver
    end
  end
  
  def test_redirect
    startserver('--test -o')
    begin
      
      visit( "#{@rootpage}/ui5/" )
      assert_equal 'Fiveapples demo app', page.title
      assert page.has_text? 'Gloster'
      assert_equal "#{@rootpage}/ui5_1way/index.html", page.current_url
      stopserver
      
      
      startserver('--test')
# simulate a browser cached redirect from eg / to  /ui5_1way/index.html
# because the browser doesnot know we restartet the server with 
# another option
      visit( "#{@rootpage}/ui5_1way/index.html" )

      assert page.has_text? 'Gloster'
      assert_equal "#{@rootpage}/ui5/index.html", page.current_url
    ensure
      stopserver
    end
  end
end