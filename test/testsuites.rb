

require 'test/unit/testsuite'
require 'sequel'
require_relative  'test_helper.rb'

class TSBase
  def self.suite
    suites = Test::Unit::TestSuite.new("TSBase")
    suites <<  TCBase.subsuites
    suites
  end
end

class TSStandalone
  def self.suite
    suites = Test::Unit::TestSuite.new("TSStandalone")
    suites <<  TCStandalone.suite
    suites
  end
end

class TSLogging
  def self.suite
    suites = Test::Unit::TestSuite.new("TSLogging")
    suites <<  BasicNavigationTestCase.suite
    suites
  end
end

class TSMini
  def self.suite
    suites = Test::Unit::TestSuite.new("TSMini")
    suites <<  TCMini.subsuites
    suites
  end
end

