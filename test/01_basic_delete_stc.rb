
require_relative './test_helper.rb'

require 'timeout'

class BasicDeleteTestCase < TCBase
  class << self
    def startup
# Sequel Models are loaded in another thread, we need to wait for them
      WaitFor.constant_loaded{ Breeder }
    
      b = Breeder.new
      b.first_name = 'Test_f'
      b.last_name = 'Test_l'
      b.save
    end
  end
  def setup
    @startpage = "http://#{FiveA.host.to_s}:9494/#{FiveA.ui5d}"
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
  
  def teardown
#    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
  
  def test_test
    assert true
  end
  
  def test_click_cultivar_delete
    WaitFor.constant_loaded{Cultivar}
    cult = Cultivar.find( :name => 'Lord Lambourne' )
    visit(@startpage)
    @detailpath = "index.html#/cultivar/#{cult.id}"
    e = page.find('span', text: 'Lord Lambourne')
    
    assert_path_after_click(@detailpath) {e.click}
    nbcount = Cultivar.count
    
    b = page.find_button 'Delete'
    assert_path_after_click(@detailpath) {b.click}
    
    # confirm deletion
    b = page.find_button 'Confirm'
    assert_path_after_click("index.html#/cultivarList") {b.click}
    # check success messagetoast    MessageToast.show("Delete success");
    assert page.has_content? 'Delete success'
    
    # give some time to reach the backend
    expected_cnt = nbcount - 1
    WaitFor::assert_equal(expected_cnt) { Cultivar.count }    
  end

  ## this is with FK contraint error raised by sqlite
  ## because the breeder record has cultivars assigned
  ## DONE--> make it display an corresponding error message 
  ## TODO --> make it display a more user friendly error ?
  def test_click_breeder_delete_err_handling
    
    WaitFor.constant_loaded{Breeder}
    brd = Breeder.find( :last_name => 'Grieve' )
    
    @detailpath = "index.html#/breeder/#{brd.id}"
    visit(@startpage)
    
    e = page.find('li', text: 'List of Breeders')
    assert_path_after_click('index.html#/breederList') { e.click }
    
    e = page.find('span', text: 'Grieve')
    
    assert_path_after_click(@detailpath) {e.click}

    nbcount = Breeder.count
    
    b = page.find_button 'Delete'
    assert_path_after_click(@detailpath) {b.click}
    
    # confirm deletion
    b = page.find_button 'Confirm'
    # Note: Deletion should fail, and we stay on same page
    assert_path_after_click(@detailpath) {b.click}
    
    # check that the deletion failure error message is displayed
    assert  page.has_text?(/Delete\sfailed/)
    
    # give some time to reach the backend and check that there was no deletion
    expected_cnt = nbcount 
    WaitFor.assert_equal(expected_cnt) { Breeder.count }    
  end

  def test_click_breeder_delete
    # Sequel Models are loaded in another thread, we need to wait for them
    WaitFor.constant_loaded{Breeder}
    
    brd = Breeder.find( :first_name => 'Test_f' )
    
    visit(@startpage)
    @detailpath = "index.html#/breeder/#{brd.id}"
    
    e = page.find('li', text: 'List of Breeders')
    
    assert_path_after_click('index.html#/breederList') { e.click }
    
    e = page.find('span', text: 'Test_f')
    
    assert_path_after_click(@detailpath) {e.click}
    nbcount = Breeder.count
    
    b = page.find_button 'Delete'
    assert_path_after_click(@detailpath) {b.click}
    
    # confirm deletion
    b = page.find_button 'Confirm'
    assert_path_after_click("index.html#/breederList") {b.click}
    
    # check success messagetoast    MessageToast.show("Delete success");
    assert page.has_content? 'Delete success'
    
# give some time to reach the backend
    expected_cnt = nbcount - 1
    WaitFor.assert_equal(expected_cnt) { Breeder.count }    
  end
  
end