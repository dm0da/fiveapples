
require_relative './test_helper.rb'

require 'timeout'
require 'json'


# a somewhat more complex testcase with navigation + upload Avatar combined
# This probably duplicates partly some other simpler tests
# but also add's some unique coverage, eg. checking Avatar image in cultivar
# after having uploaded it in the Breeder Detail page

# 0. start with a Cultivar havin a Breeder with a name (initials) 
#    but without photo
# 1.a go to Cult. Detail page, 
# 1.b    check Breeder Avatar is with the Initials
# 2.a navigate to the Breeder Detail page by clicking on the link
# 2.b  check Breeder Avatar is with the Initials
# 3.a  upload Breeder Avatar photo
# 3.b   check Breeder Avatar is with the uploaded photo
# 4.a navigate back, and check that we end up in the Cultivar Detail page
# 4.b  check Breeder Avatar is with the uploaded photo 

class UploadAvatarNavigationTestCase < TCBase
  def setup
    @startpage = "http://#{FiveA.host.to_s}:9494/#{FiveA.ui5d}"
    Capybara.reset_sessions!
    Capybara.use_default_driver
    
    @site = "http://#{FiveA.host.to_s}:9494"
    @oroot = "http://#{FiveA.host.to_s}:9494/odata"

# step #0 create test-data      
      WaitFor.constant_loaded{ Breeder }
      WaitFor.constant_loaded{ Cultivar }
      
      @b = Breeder.new
      @b.first_name = 'Test_yf0'
      @b.last_name = 'XTest_zl0'
      @b.save
      
      @c = Cultivar.new
      @c.name = 'cult_xn0'
      @c.breeder_id = @b.id
      @c.save

  end
  
  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
  
  def test_test
    assert true
  end
  
  def test_breeder_upload_avatar_navigation
    

    @detailpath = "index.html#/cultivar/#{@c.id}"

# 1.a go to Cult. Detail page, 
    
    visit("#{@startpage}/#{@detailpath}")
    assert page.find_field 'Name', with: @c.name, disabled: :all

# 1.b check Breeder Avatar is with the Initials
    dpanel = page.find(:xpath, './/div[@id="__panel0-content"]')
    assert dpanel.find('span', class: 'sapFAvatarInitialsHolder', 
                  text: 'TX',
# test doesnt work without visible all. Why ? 
# the element is actually visible ?                  
                  :visible => :all)
                  
    
# 2.a navigate to the Breeder Detail page by clicking on the link
    breeder_link = page.find('a', text: "#{@b.first_name} #{@b.last_name}")
    assert breeder_link
    @brdpath = "index.html#/breeder/#{@b.id}"
    assert_path_after_click(@brdpath) { breeder_link.click }
    
    assert page.find_field name: 'first_name', with: @b.first_name
    assert page.find_field name: 'last_name', with: @b.last_name

# 2.b  check Breeder Avatar is with the Initials
  # Warning; fragile test, ui5 internals here...
  # __panel4-content because the previously visited cultivar page
  # had __panel0 to __panel3  ... 
  
  apanel = page.find(:xpath, './/div[@id="__panel4-content"]')
    assert apanel.find('span', class: 'sapFAvatarInitialsHolder', 
                  text: 'TX',
                  :visible => :all)
          
# 3.a  upload Breeder Avatar photo
    attach_file 'myAvatarUpload', '../test/uploadfiles/TopazeCat.png' 
    u = page.find_button 'Upload Avatar'
    assert_path_after_click(@brdpath) {u.click}

## toasted ?
    assert page.has_content? 'File Uploaded'
    # after upload the file input field shall be cleared
    f = page.find_field 'myAvatarUpload', with: ''

# 3.b   check Breeder Avatar is with the uploaded photo
# get the image media entity uri by querying odata   

  ODataClient.with_response("breeder(#{@b.id})?$expand=avatar") do |resp|
      assert_equal 200, resp.code
      assert_equal 'application/json;charset=utf-8', resp.headers[:content_type]
      @imgsrc = ODataClient.with_parsed(resp) do |jresd|
         jresd['avatar']['__metadata']['media_src']
      end
      @bgstyle = "background-image:url('#{@imgsrc}')"
    
      assert apanel.find(:xpath, '//span[@role="img"]',
                     :style =>  @bgstyle, 
                     :visible => :all)
    end 
 


# 4.a navigate back, and check that we end up in the Cultivar Detail page

  menubar = page.find('div', id: '__bar1')
  backbutton = menubar.find_button(title: 'Navigate Back', 
                                   :visible => :true,
                                   disabled: :all)
  assert backbutton
  assert_path_after_click(@detailpath) {backbutton.click}

# 4.b  check Breeder Avatar is with the uploaded photo 
# get Detail content div of cultivar page 
  dpanel = page.find(:xpath, './/div[@id="__panel0-content"]')
  
# TODO this is working fine in capybara/selnium but not in real browsing ?  
  assert dpanel.has_xpath?('//span[@role="img"]',
                     :style =>  @bgstyle, 
                     :visible => :true)

   
    
  end
 
end