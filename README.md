# Fiveapples

Fiveapples is a fully opensource and standalone OData + openui5 demo app. It runs a
sqlite database, serves it as a [Safrano](https://gitlab.com/dm0da/safrano) based OData endpoint
and additionally serves the openui5 demo app itself, all in a single ruby process.

## Installation

```bash
$ gem install fiveapples
```

Starting from version 0.0.5, the gem includes a full openui5 runtime (about 27 Mb)
thus the installation can take a few seconds.


## Usage

`fiveapples` is a standalone executable running the database, the odata service
and the ui5 app all in one. Just run the executable in a terminal.
On first start it will copy a small demo database with some content into your
home directory to `~/.config/fiveapples/fiveapples.db3`
The ui5 app and the odata service will use this database in your home directory and you can use
the app to display, create, change, delete records.

```bash
$ fiveapples
Fiveapples version 0.0.5 on localhost 
Thin web server (v1.7.2 codename Bachmanity)
Maximum connections set to 1024
Listening on 0.0.0.0:9494, CTRL+C to stop
```

Then you can test in you browser:
* the standalone ui5 app: http://localhost:9494/ui5/index.html
* the odata service: http://localhost:9494/odata/$metadata

## UI5 app features

* Split-app Menu-list
* Table views with filtering, sorting and displaying related attributes by using $expand
* create new records
* navigation to detail views with edit/delete records
* simple select dialogs (search helps) fragments
* Uses TwoWay binding per default. A OneWay/no-batch version is provided as well for didactic purpose. New features will be added in the TwoWay version only.
* Uses sap.m.Image, LightBox and Avatar with OData Media Entity upload and display  

## Options

`fiveapples` can be run with the following options:

```bash
-h, --help             Display this help message.
-o, --one_way          Serves with OneWay binding and without $batch. Default is TwoWay binding with $batch
-c, --use_cdn          Use the openui5 CDN instead of local resource files. Default is using local resources
-I, --libdir LIBDIR    Include LIBDIR in the search path for required modules.
```


## Technical details

* the required ui5 app files and the odata setup files are shipped as a rubigem.
* the odata service uses a lightweight sqlite database. On first start it will copy a small demo database with some content into your home directory to `~/.config/fiveapples/fiveapples.db3`
* the odata service is provided by [Safrano](https://gitlab.com/dm0da/safrano)
* a Rack configuration file `config5.ru` that serves  the odata service and the ui5 app is provided
  - odata service is accessible under http://localhost:9494/odata
  - ui5 app is accessible under http://localhost:9494/ui5/index.html
  - the `thin` ruby webserver is used as webserver
* the standalone executable is just a wrapper around `rackup config5.ru`, ie. it starts the thin webserver serving odata and ui5
* An openui5 runtime is included and installed locally and used per default. Optionally you can run `fiveapples` with the openui5 CDN ressources https://openui5.hana.ondemand.com by using the `-c, --use_cdn` option
* Starting from version 0.0.5 we have a Selenium testsuite for testing the basic navigation and CRUD functionality.


## Caveat

The ui5 app does not follow accurately the ui5 guidelines, as this would be quite a bit of additional work. The focus of `fiveapples` is more on demonstrating
a [Safrano](https://gitlab.com/dm0da/safrano) based OData client app.

## Screenshot

![Screenshot of UI5 Fiveapples app in browser](images/screenshot_ui5.png "Screenshot of UI5 Fiveapples app in browser")
