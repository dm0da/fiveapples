# frozen_string_literal: true

require 'socket'

# Ruby 2.5 ships String#delete_prefix but lets try to support 2.4
class String
  def del_prefix(prf)
    if self[0] == prf
      self[1..-1]
    end
  end
end

module FiveApples
  def self.test_host_ip
    net = Socket.ip_address_list.detect(&:ipv4_private?)
    net.nil? ? 'localhost' : net.ip_address
  end

  def self.test_localhost_ip
    net = Socket.ip_address_list.detect(&:ipv4_loopback?)
    net.nil? ? 'localhost' : net.ip_address
  end

end
