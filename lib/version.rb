# frozen_string_literal: true

module FiveApples
  VERSION = '0.0.8'
end

module OpenUI5
  VERSION = '1.60.23'
end
