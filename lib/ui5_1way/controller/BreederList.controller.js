sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/model/odata/ODataModel",
    "sap/ui/commons/Dialog",
    "sap/ui/model/resource/ResourceModel"
  ],
  function(Controller, MessageToast, ODataModel, Dialog, ResourceModel) {
    "use strict";
    return Controller.extend("fivea.controller.BreederList", {
      onCreatePress: function(evt) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("breeder_create");
      },

      onDeletePress: function(evt) {
        var oTable = this.getView().byId("breeder_table");
        var oModel = this.getView().getModel();
        var iIndex = oTable.getSelectedIndex();
        var mParms = {};

        var oDeleteDialog = new sap.ui.commons.Dialog();
        oDeleteDialog.setTitle("Delete breeder");
        var oText = new sap.ui.commons.TextView({
          text: "Are you sure to delete this Breeder?"
        });
        oDeleteDialog.addContent(oText);
        oDeleteDialog.addButton(
          new sap.ui.commons.Button({
            text: "Confirm",
            press: function() {
              var mParms = {};
              mParms.merge = true;

              mParms.success = function(data, response) {
                oModel.refresh();
                oDeleteDialog.close();
              };
              mParms.error = function(error) {
                oDeleteDialog.close();
                MessageToast.show("Delete failed");
              };
              var sMsg;
              if (iIndex < 0) {
                sMsg = "no item selected";
              } else {
                sMsg = oTable.getContextByIndex(iIndex);
                mParms.context = sMsg;
                oModel.remove("", mParms);
              }
            }
          })
        );
        oDeleteDialog.open();
      },
      onSelectionChange: function(evt) {
        var oTable = this.getView().byId("breeder_table");
        var iIndex = oTable.getSelectedIndex();

        var oCtxt = oTable.getContextByIndex(iIndex);
        var sToPageId = oCtxt.getProperty("id");

        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        //            MessageToast.show(" navTo cultivar_detail -->"+sToPageId);
        oRouter.navTo("breeder_detail", { id: sToPageId });
      }
    });
  }
);
