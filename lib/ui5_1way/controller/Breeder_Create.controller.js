sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/core/routing/History"
  ],
  function(Controller, MessageToast, History) {
    "use strict";
    return Controller.extend("fivea.controller.Breeder_Create", {
      onInit: function() {
        this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        this._oRouter
          .getRoute("breeder_detail")
          .attachPatternMatched(this._onDetailMatched, this);
        this.byId("last_name").setValue("");
        this.byId("first_name").setValue("");
      },
      onBeforeRendering: function() {
        this.byId("last_name").setValue("");
        this.byId("first_name").setValue("");
      },
      _onDetailMatched: function(oEvent) {
        // twoway bindingmode has to be set from start on creation of the Model,
        // otherwise it does not wotk apparently
        //                                BindingMode: "TwoWay"});
      },
      _onBindingChange: function(oEvent) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        // No data for the binding
        //if (!this.getView().getBindingContext()) {
        //	oRouter.navTo("notFound");
        //}
      },
      onNavPress: function() {
        var oHistory = History.getInstance();
        var sPreviousHash = oHistory.getPreviousHash();

        if (sPreviousHash !== undefined) {
          // history contains a previous entry
          window.history.go(-1);
        } else {
          this._oRouter.navTo("breederList");
        }
      },
      onCreate: function() {
        var myForm = sap.ui.getCore().byId("breeder_create_form");
        var sObjectPath = "/breeder";
        var oView = this.getView();
        var oModel = oView.getModel();

        var oEntry = {};

        oEntry.last_name = this.byId("last_name").getValue();
        oEntry.first_name = this.byId("first_name").getValue();

        function onSuccessHandler() {
          oModel.refresh();
          MessageToast.show("Create success");
        }
        function onErrorHandler() {
          MessageToast.show("Create failed");
        }

        oModel.create(sObjectPath, oEntry, {
          success: onSuccessHandler,
          error: onErrorHandler
        });
      },

      onNavBack: function() {
        var oHistory = History.getInstance();
        var sPreviousHash = oHistory.getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.go(-1);
        } else {
          var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          oRouter.navTo("breederList", true);
        }
      }
    });
  }
);
