sap.ui.define(
  [
    "jquery.sap.global",
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "sap/ui/commons/Dialog",
    "sap/ui/core/routing/History"
  ],
  function(jQuery, Controller, MessageToast, MessageBox, Dialog, History) {
    "use strict";
    return Controller.extend("fivea.controller.Cultivar_Detail", {
      onInit: function() {
        this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        this._oRouter
          .getRoute("cultivar_detail")
          .attachPatternMatched(this._onDetailMatched, this);
      },

      _onDetailMatched: function(oEvent) {
        this._sObjectID = oEvent.getParameter("arguments").id;
        var sObjectPath = "/cultivar(" + this._sObjectID + ")";
        this._sObjectPath = sObjectPath;
        console.log(
          "Cultivar_Detail _onDetailMatched sObjectPath = " + sObjectPath
        );
        var oView = this.getView();
        oView.bindElement({ path: sObjectPath });
        var oModel = oView.getModel();
      },
      _onBindingChange: function(oEvent) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
      },
      onCreatePress: function(evt) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("cultivar_create");
      },

      onDeletePress: function(evt) {
        var oView = this.getView();
        var oModel = oView.getModel();

        var mParms = {};

        var oDeleteDialog = new sap.ui.commons.Dialog();
        oDeleteDialog.setTitle("Delete Cultivar");
        var oText = new sap.ui.commons.TextView({
          text: "Are you sure to delete this Cultivar?"
        });
        var _that = this;
        oDeleteDialog.addContent(oText);
        oDeleteDialog.addButton(
          new sap.ui.commons.Button({
            text: "Confirm",
            press: function() {
              var mParms = {};

              mParms.success = function(data, response) {
                oDeleteDialog.close();
                _that._oRouter.navTo("cultivarList");
                oModel.refresh();
              };
              mParms.error = function(error) {
                oDeleteDialog.close();
                MessageToast.show("Delete failed");
              };
              var sMsg;
              sMsg = oView.getBindingContext();
              mParms.context = sMsg;
              oModel.remove("", mParms);
            }
          })
        );
        oDeleteDialog.open();
      },
      onSave: function() {
        var oView = this.getView();
        var oModel = oView.getModel();
        var sPath = this._sObjectPath;
        var mParms = {};
        var oEntry = {};

        var oData = oModel.getData(sPath);

        oEntry.name = this.byId("name").getValue();
        oEntry.year = this.byId("year").getValue();
        oEntry.breeder_id = this.byId("breeder_id").getValue();

        function onSuccessHandler() {
          oModel.refresh();
          oModel.updateBindings();
          MessageToast.show("Update success");
        }
        function onErrorHandler() {
          MessageToast.show("Update failed");
        }

        mParms.success = onSuccessHandler;
        mParms.error = onErrorHandler;
        mParms.merge = false;
        oModel.setTokenHandlingEnabled(true);
        oModel.updateSecurityToken();
        oModel.update(this._sObjectPath, oEntry, mParms);
      },
      onCreateParentForPress: function() {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("parent_create_for", { cultivar_id: this._sObjectID });
      },
      handleBreederPress: function() {
        var sBreeder_id = this.byId("breeder_id").getValue();
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("breeder_detail", { id: sBreeder_id });
      },

      onCheck: function() {
        var oView = this.getView();
        var oModel = oView.getModel();
        var sPath = this._sObjectPath;
        var oData = oModel.getData(sPath);
        console.log("Model data:", oData);
        console.log("Model has pending changes:", oModel.hasPendingChanges());
        console.log("Pending changes:", oModel.getPendingChanges());
      },

      onNavBack: function() {
        var oHistory = History.getInstance();
        var sPreviousHash = oHistory.getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.go(-1);
        } else {
          var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          oRouter.navTo("cultivarList", true);
        }
      },
      onParentSelectionChange: function() {
        var oTable = this.byId("parent_table");
        var iIndex = oTable.getSelectedIndex();

        var oCtxt = oTable.getContextByIndex(iIndex);
        var sToPageParID = oCtxt.getProperty("parent_id");
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("cultivar_detail", { id: sToPageParID });
      },
      onChildSelectionChange: function() {
        var oTable = this.byId("child_table");
        var iIndex = oTable.getSelectedIndex();

        var oCtxt = oTable.getContextByIndex(iIndex);
        var sToPageID = oCtxt.getProperty("cultivar_id");
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("cultivar_detail", { id: sToPageID });
      },
      // Warning: alot of dupl. code with the "cultivar create" controlller
      inputId: "",
      inputDescrId: "",
      onBreederCreatePress: function(evt) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("breeder_create");
      },
      onBreederIdValueHelp: function(oEvent) {
        this.inputId = oEvent.getSource().getId();
        this.inputDescrId = this.inputId + "_descr";

        // create value help dialog
        if (!this._valueHelpDialog) {
          this._valueHelpDialog = sap.ui.xmlfragment(
            "fivea.view.BreederOptio",
            this
          );

          this.getView().addDependent(this._valueHelpDialog);
        }

        this._valueHelpDialog.open("");
      },

      _handleValueHelpSearch: function(evt) {
        var sValue = evt.getParameter("value");
        var oFilter = new Filter(
          "name",
          sap.ui.model.FilterOperator.Contains,
          sValue
        );
        evt
          .getSource()
          .getBinding("items")
          .filter([oFilter]);
      },

      _handleValueHelpClose: function(evt) {
        var oSelectedItem = evt.getParameter("selectedItem");
        if (oSelectedItem) {
          var IDInput = this.getView().byId(this.inputId);
          IDInput.setValue(oSelectedItem.getDescription());

          var oText = this.getView().byId(this.inputDescrId);
          oText.setText(oSelectedItem.getTitle());
        }
        evt
          .getSource()
          .getBinding("items")
          .filter([]);
      },
      handleUploadComplete: function() {
        sap.m.MessageToast.show("File Uploaded");
        //            var oFilerefresh = this.getView().byId("itemlist");
        //            oFilerefresh.getModel("Data").refresh(true);
        //            sap.m.MessageToast.show("File refreshed");
      },
      handleUploadPress: function() {
        var oFileUploader = this.getView().byId("fileUploader");
        if (oFileUploader.getValue() === "") {
          MessageToast.show("Please Choose any File");
        }
        oFileUploader.addHeaderParameter(
          new sap.ui.unified.FileUploaderParameter({
            name: "SLUG",
            value: oFileUploader.getValue()
          })
        );
        oFileUploader.setSendXHR(true);

        oFileUploader.upload();
      }
    });
  }
);
