sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "sap/ui/model/Filter",
    "sap/ui/model/odata/ODataModel",
    "sap/ui/core/routing/History",
    "./FiveaBaseController"
  ],
  function(
    Controller,
    MessageToast,
    Fragment,
    Filter,
    ODataModel,
    History,
    FiveaBaseController
  ) {
    "use strict";
    return FiveaBaseController.extend("fivea.controller.Parentage_Create", {
      _attachDetailMatched: function() {
        this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        this._oRouter
          .getRoute("parentage_create_for")
          .attachPatternMatched(this._onDetailMatched, this);
      },
      _onDetailMatched: function(oEvent) {
        var oObject = oEvent.getParameter("arguments");
        var sCultivarID = oObject.cultivar_id;
        var sObjPath = "/cultivar(" + sCultivarID + ")";
        var sAttribPath = "/cultivar(" + sCultivarID + ")/name";

        var oView = this.getView();

        var oModel = this.getView().getModel();
        var oRIDtxt = this.byId("cultivar_id_descr");

        function onSuccessHandler() {
          var sRtxt = oModel.getProperty(sAttribPath);
          oRIDtxt.setText(sRtxt);
        }
        function onErrorHandler() {
          MessageToast.show("Read failed");
        }

        oModel.read(sObjPath, {
          success: onSuccessHandler,
          error: onErrorHandler
        });

        var oRID = this.byId("cultivar_id");
        oRID.setValue(sCultivarID);
        //
        // twoway bindingmode has to be set from start on creation of the Model,
        // otherwise it does not wotk apparently
        //                                BindingMode: "TwoWay"});
      },
      _onBindingChange: function(oEvent) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
      },
      onCreate: function() {
        var myForm = sap.ui.getCore().byId("parentage_create_form");
        var sObjectPath = "/parentage";
        var oView = this.getView();
        var oModel = oView.getModel();

        var oEntry = {};

        var _that = this;
        oEntry.cultivar_id = this.byId("cultivar_id").getValue();
        oEntry.ptype_id = this.byId("ptype_id").getValue();
        oEntry.parent_id = this.byId("parent_id").getValue();

        function onSuccessHandler() {
          oModel.refresh();
          _that.byId("cultivar_id").setValue("");
          _that.byId("ptype_id").setValue("");
          _that.byId("parent_id").setValue("");
          MessageToast.show("Create success");
          _that.onNavBack();
        }
        function onErrorHandler() {
          MessageToast.show("Create failed");
        }

        oModel.create(sObjectPath, oEntry, {
          success: onSuccessHandler,
          error: onErrorHandler
        });
      },

      onNavBack: function() {
        var oHistory = History.getInstance();
        var sPreviousHash = oHistory.getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.go(-1);
        } else {
          var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          oRouter.navTo("ParensList", true);
        }
      },
      inputId: "",
      inputDescrId: "",

      onIdValueHelp: function(oEvent) {
        this.inputId = oEvent.getSource().getId();
        this.inputDescrId = this.inputId + "_descr";

        // create value help dialog
        if (!this._valueHelpDialog) {
          this._valueHelpDialog = sap.ui.xmlfragment(
            "fivea.view.CultivarOptio",
            this
          );

          this.getView().addDependent(this._valueHelpDialog);
        }

        this._valueHelpDialog.open("");
      },

      _handleValueHelpSearch: function(evt) {
        var sValue = evt.getParameter("value");
        var oFilter = new Filter(
          "name",
          sap.ui.model.FilterOperator.Contains,
          sValue
        );
        evt
          .getSource()
          .getBinding("items")
          .filter([oFilter]);
      },

      _handleValueHelpClose: function(evt) {
        var oSelectedItem = evt.getParameter("selectedItem");
        if (oSelectedItem) {
          var IDInput = this.getView().byId(this.inputId);
          IDInput.setValue(oSelectedItem.getDescription());

          var oText = this.getView().byId(this.inputDescrId);
          oText.setText(oSelectedItem.getTitle());
        }
        evt
          .getSource()
          .getBinding("items")
          .filter([]);
      }
    });
  }
);
