sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/model/odata/ODataModel",
    "sap/ui/commons/Dialog",
    "sap/ui/model/resource/ResourceModel",
    "./FiveaBaseController"
  ],
  function(
    Controller,
    MessageToast,
    ODataModel,
    Dialog,
    ResourceModel,
    FiveaBaseController
  ) {
    "use strict";
    return FiveaBaseController.extend("fivea.controller.ParentageList", {
      onCreatePress: function(evt) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("parentage_create");
      },
      onSelectionChange: function(evt) {
        var oTable = this.getView().byId("parentage_table");
        var iIndex = oTable.getSelectedIndex();

        var oCtxt = oTable.getContextByIndex(iIndex);
        var sToPageRID = oCtxt.getProperty("cultivar_id");
        var sToPagePtID = oCtxt.getProperty("ptype_id");
        var sToPageParID = oCtxt.getProperty("parent_id");

        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);

        oRouter.navTo("parentage_detail", {
          cultivar_id: sToPageRID,
          ptype_id: sToPagePtID,
          parent_id: sToPageParID
        });
      },

      //onDisplayPress: function (evt) {
      //// was same as onSelectionChange now
      //} ,
      onDeletePress: function(evt) {
        var oTable = this.getView().byId("parentage_table");
        var oModel = this.getView().getModel();
        var iIndex = oTable.getSelectedIndex();
        var mParms = {};

        var oDeleteDialog = new sap.ui.commons.Dialog();
        oDeleteDialog.setTitle("Delete Parentage");
        var oText = new sap.ui.commons.TextView({
          text: "Are you sure to delete this Parentage?"
        });
        oDeleteDialog.addContent(oText);
        oDeleteDialog.addButton(
          new sap.ui.commons.Button({
            text: "Confirm",
            press: function() {
              var mParms = {};
              mParms.merge = true;

              mParms.success = function(data, response) {
                oModel.refresh();
                oDeleteDialog.close();
              };
              mParms.error = function(error) {
                oDeleteDialog.close();
                MessageToast.show("Delete failed");
              };
              var sMsg;
              if (iIndex < 0) {
                sMsg = "no item selected";
              } else {
                sMsg = oTable.getContextByIndex(iIndex);
                mParms.context = sMsg;
                oModel.remove("", mParms);
              }
            }
          })
        );
        oDeleteDialog.open();
      }
    });
  }
);
