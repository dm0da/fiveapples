sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/model/odata/ODataModel",
    "sap/ui/commons/Dialog",
    "sap/ui/model/resource/ResourceModel"
  ],
  function(Controller, MessageToast, ODataModel, Dialog, ResourceModel) {
    "use strict";
    return Controller.extend("FiveaBaseController", {
      onInit: function() {}
    });
  }
);
