sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "sap/ui/model/Filter",
    "sap/ui/model/odata/ODataModel",
    "sap/ui/core/routing/History",
    "./FiveaBaseController"
  ],
  function(
    Controller,
    MessageToast,
    Fragment,
    Filter,
    ODataModel,
    History,
    FiveaBaseController
  ) {
    "use strict";
    return FiveaBaseController.extend("fivea.controller.Parentage_Detail", {
      onInit: function() {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter
          .getRoute("parentage_detail")
          .attachMatched(this.onDetailMatched, this);
      },

      onDetailMatched: function(oEvent) {
        var oObject = oEvent.getParameter("arguments");
        var sObjectKey =
          "cultivar_id='" +
          oObject.cultivar_id +
          "',ptype_id='" +
          oObject.ptype_id +
          "',parent_id='" +
          oObject.parent_id +
          "'";
        var sObjectPath = "/parentage(" + sObjectKey + ")";
        this._sObjectPath = sObjectPath;
        var oView = this.getView();
        var oModel = sap.ui.getCore().getModel();
        oView.setModel(oModel);
        oView.bindElement({ path: sObjectPath });
      },
      _onBindingChange: function(oEvent) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
      },
      onSave: function() {
        var oView = this.getView();
        var oModel = oView.getModel();
        var sPath = this._sObjectPath;
        var mParms = {};

        var oData = oModel.getData(sPath);
        function onSuccessHandler() {
          oModel.refresh();
          MessageToast.show("Update success");
        }
        function onErrorHandler() {
          MessageToast.show("Update failed");
        }

        mParms.success = onSuccessHandler;
        mParms.error = onErrorHandler;
        mParms.merge = false;
        //           oModel.setTokenHandlingEnabled(true);
        //           oModel.updateSecurityToken();
        oModel.update(this._sObjectPath, oData, mParms);
      },

      onNavBack: function() {
        var oHistory = History.getInstance();
        var sPreviousHash = oHistory.getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.go(-1);
        } else {
          var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          oRouter.navTo("ParentageList", true);
        }
      },
      inputId: "",
      inputDescrId: "",

      onIdValueHelp: function(oEvent) {
        this.inputId = oEvent.getSource().getId();
        this.inputDescrId = this.inputId + "_descr";

        // create value help dialog
        if (!this._valueHelpDialog) {
          this._valueHelpDialog = sap.ui.xmlfragment("fivea.view.Dialog", this);

          this.getView().addDependent(this._valueHelpDialog);
        }

        this._valueHelpDialog.open("");
      },

      _handleValueHelpSearch: function(evt) {
        var sValue = evt.getParameter("value");
        var oFilter = new Filter(
          "name",
          sap.ui.model.FilterOperator.Contains,
          sValue
        );
        evt
          .getSource()
          .getBinding("items")
          .filter([oFilter]);
      },

      _handleValueHelpClose: function(evt) {
        var oSelectedItem = evt.getParameter("selectedItem");
        if (oSelectedItem) {
          var IDInput = this.getView().byId(this.inputId);
          IDInput.setValue(oSelectedItem.getTitle());

          var oText = this.getView().byId(this.inputDescrId);
          oText.setText(oSelectedItem.getDescription());
        }
        evt
          .getSource()
          .getBinding("items")
          .filter([]);
      }
    });
  }
);
