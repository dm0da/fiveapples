sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/core/routing/History"
  ],
  function(Controller, MessageToast, History) {
    "use strict";
    return Controller.extend("fivea.controller.Breeder_Detail", {
      onInit: function() {
        this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        this._oRouter
          .getRoute("breeder_detail")
          .attachPatternMatched(this._onDetailMatched, this);
      },
      _onDetailMatched: function(oEvent) {
        this._sObjectID = oEvent.getParameter("arguments").id;
        var sObjectPath = "/breeder(" + this._sObjectID + ")";
        this._sObjectPath = sObjectPath;
        var oView = this.getView();
        var oModel = sap.ui.getCore().getModel();
        oView.setModel(oModel);
        oView.bindElement({ path: sObjectPath });
      },
      _onBindingChange: function(oEvent) {},
      onCultivarSelectionChange: function() {
        var oTable = this.byId("breeder_cultivar_table");
        var iIndex = oTable.getSelectedIndex();

        var oCtxt = oTable.getContextByIndex(iIndex);
        var sToPageParID = oCtxt.getProperty("id");
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("cultivar_detail", { id: sToPageParID });
      },

      onSave: function() {
        var oView = this.getView();
        var oModel = oView.getModel();
        var sPath = this._sObjectPath;
        var mParms = {};
        var oEntry = {};

        var oData = oModel.getData(sPath);

        oEntry.last_name = this.byId("last_name").getValue();
        oEntry.first_name = this.byId("first_name").getValue();

        function onSuccessHandler() {
          oModel.refresh();
          oModel.updateBindings();
          MessageToast.show("Update success");
        }
        function onErrorHandler() {
          MessageToast.show("Update failed");
        }
        console.log("Model data:", oData);
        console.log("View data:", oEntry);
        console.log("Model has pending changes:", oModel.hasPendingChanges());
        console.log("Pending changes:", oModel.getPendingChanges());

        mParms.success = onSuccessHandler;
        mParms.error = onErrorHandler;
        mParms.merge = false;
        oModel.setTokenHandlingEnabled(true);
        oModel.updateSecurityToken();
        oModel.update(this._sObjectPath, oEntry, mParms);
      },

      onCheck: function() {
        var oView = this.getView();
        var oModel = oView.getModel();
        var sPath = this._sObjectPath;
        var oData = oModel.getData(sPath);
        console.log("Model data:", oData);
        console.log("Model has pending changes:", oModel.hasPendingChanges());
        console.log("Pending changes:", oModel.getPendingChanges());
      },

      onNavBack: function() {
        var oHistory = History.getInstance();
        var sPreviousHash = oHistory.getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.go(-1);
        } else {
          var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          oRouter.navTo("breederList", true);
        }
      }
    });
  }
);
