#!/usr/bin/env ruby

require 'sequel'
require 'fileutils'
require 'erb'
require_relative 'version'
require_relative 'fa_utils'

require 'safrano.rb'
require 'puma/cli'


# temp. bugfix for old Rack 2.0 and safrano 0.6.7 
if ( Rack.release[0..2] == '2.0' ) && ( Safrano::VERSION == '0.6.7' )
  module Rack
    Files = File
  end
end

module FiveApples
  class Server
    CONFIGDIRNAME = 'fiveapples'.freeze
    DBFILENAME = 'fiveapples.db3'.freeze
    CONFIGRU = 'config.ru'.freeze
    MEDIADIR = 'media'.freeze

    attr_reader :db
    attr_reader :one_way
    attr_reader :ui5d
    attr_reader :resources
    attr_reader :role
    attr_reader :host
    attr_reader :port
    attr_reader :media_root
    attr_accessor :log

    def initialize(options = {})
      options ||= {}
      @role = options[:role]
      # from libdir we readonly. --> can be a system (install) dir
      @libdir = __dir__.dup

      # apphome needs to be user-writeable !
      @apphome = if (@role == :test)
                   File.expand_path('testdb', @libdir)
                 else
                   gconfigdir = File.expand_path('.config', Dir.home)
                   File.expand_path(CONFIGDIRNAME, gconfigdir)
      end

      $LOAD_PATH.unshift(@libdir) unless $LOAD_PATH.include?(@libdir)

      @systemdbname = File.join(@libdir, DBFILENAME)
      @homedbname = File.expand_path(DBFILENAME, @apphome)
      @media_root = File.expand_path(MEDIADIR, @apphome)
      @configru = File.expand_path(CONFIGRU, @libdir)
      @one_way = options[:one_way]
      @use_cdn = options[:use_cdn]

      # in testmode we dont log per default,
      # in non testmode we log per default,
      #  but this can be overriden with option
      @log = (options[:log] || (@role != :test))

      @resources = @use_cdn ? "https://openui5.hana.ondemand.com/#{OpenUI5::VERSION}/resources/sap-ui-core.js" : '../resources/sap-ui-core.js'

      @ui5d = @one_way ? 'ui5_1way' : 'ui5'
      @port = 9494
      @host = if ((@role == :test) && ENV['selenium_remote_url'])
                FiveApples.test_host_ip()
              else
                FiveApples.test_localhost_ip()  
                
              end
              
    end

    def setup_db_in_homedir
      FileUtils.mkdir_p(@apphome) unless Dir.exist?(@apphome)
      if (@role == :test)
        # in test-mode always start with a fresh DB copy
        FileUtils.cp(@systemdbname, @homedbname)

        # and reset the media directory
        media_photo_dir = File.absolute_path('Photo', @media_root)
        FileUtils.remove_entry_secure(media_photo_dir) if File.exist?(media_photo_dir)
      else
        FileUtils.cp(@systemdbname, @homedbname) unless File.exist?(@homedbname)
      end
    end

    def db_create_photo_table
      @db.create_table :photo do
        primary_key :id
        column :name, :text
        Integer :cultivar_id
        column :content_type, :text
      end
    end

    def db_breeder_table_photo_id
      return if @db.schema(:breeder).find { |col| col[0] == :photo_id }

      @db.alter_table :breeder do
        add_column :photo_id, Integer
        add_foreign_key [:photo_id], :photo, key: :id,
                                             name: :breeder_photo_id_fkey
      end
    end

    def startdb
      setup_db_in_homedir

      @db = Sequel::Model.db = Sequel.sqlite(@homedbname)
      # data model for media entities table and relations
      unless @db.table_exists? :photo
        db_create_photo_table
      end

      db_breeder_table_photo_id

      Kernel.at_exit { Sequel::Model.db.disconnect if Sequel::Model.db }
    end

    # needed for ERB
    def get_binding
      binding
    end

    def serve
      puts "Fiveapples version #{FiveApples::VERSION} on #{@host} config #{@configru}"
      begin
        # this requires safrano ~> 0.3.5
        puts "Safrano version #{Safrano::VERSION}"
      rescue
      end
      
      puts "libdir is #{@libdir}" 
      puts "configru is #{@configru}" 
      #       this is equivalent to  'puma config.ru' but in same process
      argv = ['--no-config',"--dir","#{@libdir}", "--bind", "tcp://#{@host}:#{@port}", @configru]
      cli = Puma::CLI.new argv
      cli.run
    end
  end
end
