sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "./Detail",
    "./Breeder",
    "./Create"
  ],
  function(Controller, MessageToast, Detail, Breeder, Create) {
    "use strict";
    return Controller.extend("fivea.controller.Cultivar_Create", {
      onInit: function() {
        this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        this._oRouter
          .getRoute("cultivar_create")
          .attachPatternMatched(this.createEntry, this);      
      },
      createEntry: function(){
        var oView = this.getView();
        var oModel = this.getOwnerComponent().getModel();
        var oContext = oModel.createEntry("/cultivar", {});
        oView.setModel(oModel);
        oView.setBindingContext(oContext);  
      },
      onCreatePress: function() {  
        Create.onPress(this);
      },
      navToDetail: function(oData){
        // navigate to detail page but dont keep create 
        // page history (hist. replace)        
        
        this._oRouter.navTo("cultivar_detail", {  id: oData.id   }, {}, true);
      },
      navToList: function(){
        this._oRouter.navTo("cultivarList", true);
      },
      onNavBack: function() {
        Detail.onNavBack(this);
      },
      inputId: "",
      inputDescrId: "",
      onBreederIdValueHelp: function(oEvent) {
        Breeder.onIdValueHelp(this, oEvent);
      },

      _handleValueHelpSearch: function(evt) {
        Breeder._handleValueHelpSearch(evt);
      },

      _handleValueHelpClose: function(evt) {
        Breeder._handleValueHelpClose(this, evt);
      }
    });
  }
);
