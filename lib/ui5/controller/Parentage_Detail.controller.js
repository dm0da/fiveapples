sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "sap/ui/model/Filter",
    "sap/ui/model/odata/ODataModel",
    "sap/ui/core/routing/History",
    "./Detail"
  ],
  function(Controller, MessageToast, Fragment, Filter, ODataModel, 
           History, Detail) {
    "use strict";
    return Controller.extend("fivea.controller.Parentage_Detail", {
      onInit: function() {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter
          .getRoute("parentage_detail")
          .attachMatched(this.onDetailMatched, this);
      },

      onDetailMatched: function(oEvent) {
        var oObject = oEvent.getParameter("arguments");
        var sObjectKey =
          "cultivar_id=" +
          oObject.cultivar_id +
          ",ptype_id=" +
          oObject.ptype_id +
          ",parent_id=" +   oObject.parent_id ;
        var sObjectPath = "/parentage(" + sObjectKey + ")";

        this._sObjectPath = sObjectPath;
        var oView = this.getView();
        var oModel = sap.ui.getCore().getModel();
        oView.setModel(oModel);
        oView.bindElement({ path: sObjectPath });
      },
      _onBindingChange: function(oEvent) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
      },
      onSave: function() {
        Detail.onSave(this);
      },
      navToList: function(){
        this._oRouter.navTo("ParentageList", true);
      },      
      onNavBack: function() {
        Detail.onNavBack(this);
      },
      
      inputId: "",
      inputDescrId: "",

      onIdValueHelp: function(oEvent) {
        this.inputId = oEvent.getSource().getId();
        this.inputDescrId = this.inputId + "_descr";

        // create value help dialog
        if (!this._valueHelpDialog) {
          this._valueHelpDialog = sap.ui.xmlfragment("fivea.view.Dialog", this);

          this.getView().addDependent(this._valueHelpDialog);
        }

        this._valueHelpDialog.open("");
      },

      _handleValueHelpSearch: function(evt) {
        var sValue = evt.getParameter("value");
        var oFilter = new Filter(
          "name",
          sap.ui.model.FilterOperator.Contains,
          sValue
        );
        evt
          .getSource()
          .getBinding("items")
          .filter([oFilter]);
      },

      _handleValueHelpClose: function(evt) {
        var oSelectedItem = evt.getParameter("selectedItem");
        if (oSelectedItem) {
          var IDInput = this.getView().byId(this.inputId);
          IDInput.setValue(oSelectedItem.getTitle());

          var oText = this.getView().byId(this.inputDescrId);
          oText.setText(oSelectedItem.getDescription());
        }
        evt
          .getSource()
          .getBinding("items")
          .filter([]);
      }
    });
  }
);
