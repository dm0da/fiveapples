sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/model/odata/ODataModel",
    "sap/ui/commons/Dialog",
    "sap/ui/model/resource/ResourceModel"
  ],
  function(Controller, MessageToast, ODataModel, Dialog, ResourceModel) {
    "use strict";
    return Controller.extend("fivea.controller.BreederList", {
      onCreatePress: function(evt) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("breeder_create");
      },
      onSelectionChange: function(evt) {
        var oTable = this.getView().byId("breeder_table");
        var iIndex = oTable.getSelectedIndex();

        var oCtxt = oTable.getContextByIndex(iIndex);
        if (oCtxt) {
          var sToPageId = oCtxt.getProperty("id");
          var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          oRouter.navTo("breeder_detail", { id: sToPageId });
        }
      }
    });
  }
);
