
/**  
* Common helper/callbacks for entity Detail controller, eg 
* reusable load/save/delete callbacks
* 
* **/

sap.ui.define(["sap/m/MessageToast",
"sap/ui/commons/Dialog",
"sap/ui/core/routing/History"], 
function(MessageToast, Dialog, History) {
   
   "use strict";

    return {
      onDeletePress: function(that, evt) {
        var oView = that.getView();
        var oModel = oView.getModel();

        var mParms = {};

        var oDeleteDialog = new sap.ui.commons.Dialog();
        oDeleteDialog.setTitle("Delete " + that._entity_name );
        var oText = new sap.ui.commons.TextView({
          text: "Are you sure to delete this " + that._entity_name
        });
        
        oDeleteDialog.addContent(oText);
        oDeleteDialog.addButton(
          new sap.ui.commons.Button({
            text: "Confirm",
            press: function() {
              var mParms = {};

              mParms.success = function(data, response) {
                MessageToast.show("Delete success");
                oDeleteDialog.close();
                that.navToList();
              };
              mParms.error = function(error) {
                oDeleteDialog.close();
                var oMsg = '';
                var errCT = error.headers['Content-Type'];
                if (errCT.indexOf('application/json') == 0 ){
                  var ojerr = JSON.parse(error.responseText);
                  oMsg = ojerr["odata.error"].message;
                }
                else{
                  oMsg = error.message + ' ' + error.statusText
                }
                MessageToast.show("Delete failed : " + oMsg);
              };
              mParms.context = oView.getBindingContext();
              oModel.remove("", mParms);
            }
          })
        );
        oDeleteDialog.open();
      },
      onSave: function(that) {
        var oView = that.getView();
        var oModel = oView.getModel();
        var mParms = {};

        function onSuccessHandler() {
          oModel.refresh();
          oModel.updateBindings();
          MessageToast.show("Update success");
        }

        function onErrorHandler() {
          MessageToast.show("Update failed");
        }

        mParms.success = onSuccessHandler;
        mParms.error = onErrorHandler;

        oModel.setTokenHandlingEnabled(true);
        oModel.updateSecurityToken();

        oModel.submitChanges(mParms);
      },
      
      onNavBack: function(that) {
        var oHistory = History.getInstance();
        var sPreviousHash = oHistory.getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.go(-1);
        } else {   
           that.navToList(); 
        }
      }   
   };
  }
);
