
sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "./FileUpload",
    "./Detail"
  ],
  function(Controller, MessageToast, FileUpload, Detail) {
    "use strict";
    return Controller.extend("fivea.controller.Breeder_Detail", {
      onInit: function() {
        this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        this._oRouter
          .getRoute("breeder_detail")
          .attachPatternMatched(this.onDetailMatched, this);
        this._entity_name = 'Breeder';
      },
      onDetailMatched: function(oEvent) {
        this._sObjectID = oEvent.getParameter("arguments").id;
        var sObjectPath = "/breeder(" + this._sObjectID + ")";
        this._sObjectPath = sObjectPath;
        var oView = this.getView();
        
        oView.bindElement({ path: sObjectPath });
        
        var oAvatar = this.byId("avatar");
        this._sAvatar = oAvatar;
        this._sAvatarImage = false;
        this.loadAvatar();
      },
      loadAvatar: function() {
        var sImgsPath = this._sObjectPath + "/avatar";
        var that = this;
        var oView = this.getView();
        var oModel = oView.getModel();
        var oAvatar = that.byId("avatar");
                   
        function _loadavsuccs(oData, response) {
          // UploadUrl for Avatar creation (first time)
          var oUrl = "/odata"+that._sObjectPath+"/avatar";
          var oSrc = null;
          if (oData.avatar !== undefined ){
            if (Object.keys(oData.avatar).length === 0) {
              var oI = oData.first_name.trim()[0] + oData.last_name.trim()[0];
              oAvatar.setInitials(oI);
            }
            else{
              oUrl = "/odata"+that._sObjectPath+"/avatar/$value";      
              oSrc = oData.avatar.__metadata.media_src;
// Avatar was already uploaded. UploadUrl should be for an Update
// but it's not yet implemented in safrano 0.4  
// (POST /Photo(4)/$value ...safrano-todo)              
              oUrl = oSrc
              
            }
          }
          oAvatar.setSrc(oSrc);
          oView.byId("fileUploader").setUploadUrl(oUrl);
          oView.byId("fileUploader").setValue("");
        }
        
        function _loadaverr(oErr) {        }
        
        oModel.read(this._sObjectPath, 
                    { urlParameters:{$expand: "avatar"}, 
                      success: _loadavsuccs, 
                      error: _loadaverr });
      },
      handleUploadComplete: function(oEvent) {
        if (FileUpload.onComplete(this, oEvent)) {
          this.loadAvatar();
        };
      },
      handleUploadPress: function() {
        FileUpload.onPress(this) ;
      },
      
      onCultivarSelectionChange: function() {
        var oTable = this.byId("breeder_cultivar_table");
        var iIndex = oTable.getSelectedIndex();

        var oCtxt = oTable.getContextByIndex(iIndex);
        var sToPageParID = oCtxt.getProperty("id");
        this._oRouter.navTo("cultivar_detail", { id: sToPageParID });
      },
      onDeletePress: function(evt) {
        Detail.onDeletePress(this, evt);
      },
      onSave: function() {
        Detail.onSave(this);
      },
      navToList: function(){
        this._oRouter.navTo("breederList", true);
      },
      onNavBack: function() {
        Detail.onNavBack(this);
      }
    });
  }
);
