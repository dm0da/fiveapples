
/**  
* Common helper/callbacks for file uploading callbacks
* 
* **/

sap.ui.define(["sap/m/MessageToast","./Base64"], 
function(MessageToast, Base64) {
   
   "use strict";

    return {
      onPress: function(that) {
        var oFileUploader = that.getView().byId("fileUploader");
        if (oFileUploader.getValue() === "") {
          MessageToast.show("Please Choose a file");
        }
// according to AtomPub spec, the SLUG has to be RFC2047-encoded 
// we use simply the B-Variant 
        var oSlugRaw = oFileUploader.getValue();
        var oSlugB2047 = `=?utf-8?b?${Base64.encode(oSlugRaw)}?=`;
        oFileUploader.destroyHeaderParameters();
        oFileUploader.addHeaderParameter(
          new sap.ui.unified.FileUploaderParameter({
            name: "SLUG",
            value: oSlugB2047
          })
        );
        oFileUploader.setSendXHR(true);

        oFileUploader.upload();
      },
      onComplete: function(that, oEvent){
        var oPars = oEvent.getParameters();
        if ( (oPars.status == 204) || (oPars.status == 201) 
                                   || (oPars.status == 200 ) ) {
          MessageToast.show("File Uploaded");
          var oModel = that.getView().getModel();
          oModel.refresh();
          return true;
        } else{
          MessageToast.show("File Upload failed ! ");
          return false;
        }
        
      }
   };
  }
);
