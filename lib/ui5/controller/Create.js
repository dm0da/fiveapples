
/**  
* Common helper/callbacks for entity creation
* 
* **/

sap.ui.define(["sap/m/MessageToast"], 
function(MessageToast) {
   
   "use strict";

    return {
      onPress: function(that) {
        
        var oModel = that.getView().getModel();
    
        function onSuccessHandler(data) {
          MessageToast.show("Create success");
          // isnt this data access OData version dependant?
          // OneWay is simply 
          // var new_entity_tab = data
                    
          // The TwoWay way. No this is not ridiculous. 
          // You tell me the better way ?

//          var entity_tab = data.__batchResponses[0].__changeResponses[0].data;
//          that.navToDetail(entity_tab.results[0]);

          var entity = data.__batchResponses[0].__changeResponses[0].data;
          that.navToDetail(entity);
        }
        function onErrorHandler() {
          MessageToast.show("Create failed");
        }
        var mParms = {};
        
        mParms.success = onSuccessHandler;
        mParms.error = onErrorHandler;

        oModel.submitChanges(mParms);
      },
   };
  }
);
