sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/model/odata/ODataModel",
    "sap/ui/commons/Dialog",
    "sap/ui/model/resource/ResourceModel"
  ],
  function(Controller, MessageToast, ODataModel, Dialog, ResourceModel) {
    "use strict";
    return Controller.extend("fivea.controller.MCultivarList", {
      typusMap: {},

      onInit: function() {},

      onCreatePress: function(evt) {
        var oCreateDialog = new Dialog();
        var oModel = this.getView().getModel();
        function onCreateSuccessHandler() {
          oCreateDialog.close();
          oModel.refresh();
        }

        function onCreateErrorHandler() {
          oCreateDialog.close();
          MessageToast.show("Create failed");
        }

        oCreateDialog.setTitle("Create Cultivar");
        var oSimpleForm = new sap.ui.layout.form.SimpleForm({
          maxContainerCols: 2,
          content: [
            new sap.ui.core.Title({ text: "New cultivar" }),
            new sap.ui.commons.Label({ text: "name" }),
            new sap.ui.commons.TextField({ value: "" })
          ]
        });
        oCreateDialog.addContent(oSimpleForm);
        oCreateDialog.addButton(
          new sap.ui.commons.Button({
            text: "Submit",
            press: function() {
              var content = oSimpleForm.getContent();
              var oEntry = {};
              oEntry.name = content[2].getValue();
              oModel.create("/cultivar", oEntry, {
                success: onCreateSuccessHandler,
                error: onCreateErrorHandler
              });
            }
          })
        );
        oCreateDialog.open();
      },

      handlePress: function(oEvent) {
        var oItem = oEvent.getSource();
        var ctx = oItem.getBindingContext();
        var sToPageId = oItem.getBindingContext().getProperty("id");

        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        //            MessageToast.show("'press' event fired! navTo cultivar_detail -->"+sToPageId);
        oRouter.navTo("cultivar_detail", { id: sToPageId });
      },

      onDeletePress: function(evt) {
        var oTable = this.getView().byId("m_cultivar_table");
        var oModel = this.getView().getModel();
        var iIndex = oTable.getSelectedIndex();

        var oDeleteDialog = new sap.ui.commons.Dialog();
        oDeleteDialog.setTitle("Delete Cultivar");
        var oText = new sap.ui.commons.TextView({
          text: "Are you sure to delete this Cultivar?"
        });
        oDeleteDialog.addContent(oText);
        oDeleteDialog.addButton(
          new sap.ui.commons.Button({
            text: "Confirm",
            press: function() {
              var mParms = {};

              mParms.success = function(data, response) {
                oModel.refresh();
                oDeleteDialog.close();
              };
              mParms.error = function(error) {
                oDeleteDialog.close();
                MessageToast.show("Delete failed");
              };
              var sMsg;
              if (iIndex < 0) {
                sMsg = "no item selected";
              } else {
                sMsg = oTable.getContextByIndex(iIndex);
                mParms.context = sMsg;
                oModel.remove("", mParms);
              }
            }
          })
        );
        oDeleteDialog.open();
      }
    });
  }
);
