sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "./Detail",
    "./Create"
  ],
  function(Controller, MessageToast, Detail, Create) {
    "use strict";
    return Controller.extend("fivea.controller.Breeder_Create", {
      onInit: function() {
        this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        this._oRouter
          .getRoute("breeder_create")
          .attachPatternMatched(this.createEntry, this);      
      },
      createEntry: function(){
        var oView = this.getView();
        var oModel = this.getOwnerComponent().getModel();
        var oContext = oModel.createEntry("/breeder", {});
        oView.setModel(oModel);
        oView.setBindingContext(oContext);  
      },
      navToList: function(){
        this._oRouter.navTo("breederList", true);
      },
      onNavBack: function() {
        Detail.onNavBack(this);
      },
      onCreatePress: function() {  
        Create.onPress(this);
      },
      navToDetail: function(oData){
        // navigate to detail page but dont keep create 
        // page history (hist. replace)        

        this._oRouter.navTo("breeder_detail", {  id: oData.id   }, {}, true);
      }
    });
  }
);
