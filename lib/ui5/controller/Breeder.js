
/**  
* Common helper/callbacks for Breeder related functions, eg 
* reusable search helps
* 
* **/

sap.ui.define(["sap/m/MessageToast",
               "sap/ui/commons/Dialog",
               "sap/ui/core/routing/History"], 
function(MessageToast, Dialog, History) {
   
   "use strict";

    return {
      
            
      onIdValueHelp: function(that, oEvent) {
        
        that.inputId = oEvent.getSource().getId();
        that.inputDescrId = that.inputId + "_descr";

        // create value help dialog
        if (!that._valueHelpDialog) {
          that._valueHelpDialog = sap.ui.xmlfragment(
            "fivea.view.BreederOptio", that );
          that.getView().addDependent(that._valueHelpDialog);
        }

        that._valueHelpDialog.open("");
      },    
      _handleValueHelpSearch: function(evt) {
        var sValue = evt.getParameter("value");
        var oFilter = new Filter(
          "name",
          sap.ui.model.FilterOperator.Contains,
          sValue
        );
        evt.getSource()
          .getBinding("items")
          .filter([oFilter]);
      },

      _handleValueHelpClose: function(that, evt) {
        var oSelectedItem = evt.getParameter("selectedItem");
        if (oSelectedItem) {
//           oSelectedItem.getBindingContextPath();
     // "/breeder(8)"
          var oModel = oSelectedItem.getModel();
          var oPath = oSelectedItem.getBindingContextPath();
          
          
          function _loadavsuccs(oData, response){
            that.loadavsuccs(oData, response);
          };
          function _loadaverr(oErr){};
          oModel.read(oPath, 
                    { urlParameters:{$expand: "avatar"}, 
                      success: _loadavsuccs, 
                      error: _loadaverr } );
          
          var IDInput = that.getView().byId(that.inputId);
          IDInput.setValue(oSelectedItem.getDescription());

          var oText = that.getView().byId(that.inputDescrId);
          oText.setText(oSelectedItem.getTitle());
        }
        evt.getSource()
          .getBinding("items")
          .filter([]);
      }      
   };
  }
);
