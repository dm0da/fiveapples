sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/model/odata/ODataModel",
    "sap/ui/model/resource/ResourceModel"
  ],
  function(Controller, MessageToast, ODataModel, ResourceModel) {
    "use strict";
    return Controller.extend("fivea.controller.CultivarList", {
      onInit: function() {},
      onCreatePress: function(evt) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("cultivar_create");
      },

      onSelectionChange: function(evt) {
        var oTable = this.getView().byId("cultivar_table");
        var iIndex = oTable.getSelectedIndex();

        var oCtxt = oTable.getContextByIndex(iIndex);
        if (oCtxt) {
          var sToPageId = oCtxt.getProperty("id");
          var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          oRouter.navTo("cultivar_detail", { id: sToPageId });
        }
      }
    });
  }
);
