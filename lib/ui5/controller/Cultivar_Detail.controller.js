

sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/commons/Dialog",
    "./FileUpload",
    "./Detail",
    "./Breeder"
  ],
  function(Controller, MessageToast, Dialog, 
           FileUpload, Detail, Breeder) {
    "use strict";
    return Controller.extend("fivea.controller.Cultivar_Detail", {
      onInit: function() {
        this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        this._oRouter
          .getRoute("cultivar_detail")
          .attachPatternMatched(this.onDetailMatched, this);
        this._entity_name = 'Cultivar';
      },
      
     onDetailMatched: function(oEvent) {
        this._sObjectID = oEvent.getParameter("arguments").id;
        this._sObjectPath = "/cultivar(" + this._sObjectID + ")";
        this._oImages = this.byId("images");
        this._oAvatar =  this.byId("avatar");
        
        this.getView().bindElement(this._sObjectPath);
        
        this.loadDetails();
        
      },
      
    
      loadImages: function(){
        
        var oModel = this.getOwnerComponent().getModel();
        var that = this;
        this._oImages.removeAllContent();
        
        function _loadimsuccs(oData, response){
          that.loadimsuccs(oData, response);
        };
        
        function _loadimerr(oErr){};
        
        oModel.read(this._sObjectPath+"/photo", 
                    { success: _loadimsuccs, 
                      error: _loadimerr });
        
      },
      loadDetails: function(){
        var oModel = this.getOwnerComponent().getModel();
        var that = this;
        this._oAvatar.setInitials(null);
        this._oAvatar.setSrc(null);
        this._oImages.removeAllContent();
        
        function _loaddetailssuccs(oData, response){
          
          that.loadimsuccs(oData.photo, response);
          that.loadavsuccs(oData.breeder, response);
          
        };
        function _loaddetailserr(oErr){};
        
        oModel.read(this._sObjectPath, 
                    { urlParameters:{$expand: "photo,breeder/avatar"}, 
                      success: _loaddetailssuccs, 
                      error: _loaddetailserr });

      },
      
      onCreatePress: function(evt) {
        this._oRouter.navTo("cultivar_create");
      },
      
      onSave: function() {
        Detail.onSave(this);
      },    
        
      onDeletePress: function(evt) {
        Detail.onDeletePress(this, evt);
      },
      
      loadavsuccs: function(oData, response) {
// this is the known photo media_src path assuming  data is already saved       
          var oUrl = "/odata"+this._sObjectPath+"/breeder/avatar/$value";
          if (oData.avatar !== undefined )
            if (Object.keys(oData.avatar).length === 0) {
              this._oAvatar.setSrc(null);
              var oI = oData.first_name.trim()[0] + oData.last_name.trim()[0];
              this._oAvatar.setInitials(oI);
            }
            else{
// this URL works when we return from the breeder search help            
              oUrl = oData.avatar.__metadata.media_src;
              this._oAvatar.setSrc(oUrl);
            }
          else{
            this._oAvatar.setSrc(null);            
          }
        },


      loadimsuccs: function(oData, response){
          var i, len, oEntry;
          var oList = oData["results"];
          for (i = 0, len = oList.length; i < len; ++i) {
            oEntry = oList[i];
            var oImage = new sap.m.Image(i);
            oImage.setSrc(oEntry.__metadata.media_src);

            oImage.setDecorative(false);
            oImage.setHeight('200px');
            var oLightBoxItem = new sap.m.LightBoxItem(i) ;
            oLightBoxItem.setImageSrc(oEntry.__metadata.media_src);
            oLightBoxItem.setSubtitle(oEntry.name);

            var oDetailBox = new sap.m.LightBox();
            oDetailBox.removeAllImageContent() ;
            oDetailBox.addImageContent(oLightBoxItem);

            oImage.setDetailBox(oDetailBox);
            this._oImages.addContent(oImage);
          }
          this.getView().byId("fileUploader").setValue("");
        
      },

      onCreateParentForPress: function() {
        this._oRouter.navTo("parent_create_for", { 
          cultivar_id: this._sObjectID }
          );
      },
      handleBreederPress: function() {
        var sBreeder_id = this.byId("breeder_id").getValue();
        this._oRouter.navTo("breeder_detail", { id: sBreeder_id });
      },

      navToList: function(){
        this._oRouter.navTo("cultivarList", true);
      },
      onNavBack: function() {
        Detail.onNavBack(this);
      },
      onParentSelectionChange: function() {
        var oTable = this.byId("parent_table");
        var iIndex = oTable.getSelectedIndex();

        var oCtxt = oTable.getContextByIndex(iIndex);
        var sToPageParID = oCtxt.getProperty("parent_id");
        this._oRouter.navTo("cultivar_detail", { id: sToPageParID });
      },
      onChildSelectionChange: function() {
        var oTable = this.byId("child_table");
        var iIndex = oTable.getSelectedIndex();

        var oCtxt = oTable.getContextByIndex(iIndex);
        var sToPageID = oCtxt.getProperty("cultivar_id");
        this._oRouter.navTo("cultivar_detail", { id: sToPageID });
      },
      onBreederCreatePress: function(evt) {
        this._oRouter.navTo("breeder_create");
      },
      // Warning: alot of dupl. code with the "cultivar create" controlller
      // --> modularised in Breeder.js
      inputId: "",
      inputDescrId: "",
      onBreederIdValueHelp: function(oEvent) {
        Breeder.onIdValueHelp(this, oEvent);
      },

      _handleValueHelpSearch: function(evt) {
        Breeder._handleValueHelpSearch(evt);
      },

      _handleValueHelpClose: function(evt) {
        Breeder._handleValueHelpClose(this, evt);
      },
      
      handleUploadComplete: function(oEvent) {
        if (FileUpload.onComplete(this, oEvent)) {
          this.loadImages();
        };
      },
      handleUploadPress: function() {
        FileUpload.onPress(this) ;
      }
    });
  }
);
