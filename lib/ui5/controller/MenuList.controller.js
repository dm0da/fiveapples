sap.ui.define(
  [
    "jquery.sap.global",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/Filter",
    "sap/ui/model/odata/ODataModel"
  ],
  function(jQuery, MessageToast, Fragment, Controller, Filter, ODataModel) {
    "use strict";

    var CController = Controller.extend("fivea.controller.MenuList", {
      onListItemPress: function(oEvent) {
        // also possible:
        // var oRouter = this.getOwnerComponent().getRouterFor(this);
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        var oItem = oEvent.getSource();
        var sToPageId = oEvent
          .getParameter("listItem")
          .getCustomData()[0]
          .getValue();
        oRouter.navTo(sToPageId);
      }
    });

    return CController;
  }
);
