#!/usr/bin/env ruby

class Cultivar < Sequel::Model(:cultivar)
  # Navigation attributes
  one_to_many :parent, :class => :Parentage, :key => :cultivar_id
  one_to_many :child, :class => :Parentage, :key => :parent_id
  one_to_many :photo, :class => :Photo, :key => :cultivar_id
  many_to_one :breeder, :key => :breeder_id
end

class ParentageType < Sequel::Model(:par_type)
end

class Breeder < Sequel::Model(:breeder)
  one_to_many :cultivar, :key => :breeder_id
  many_to_one :avatar, :class => :Photo, :key => :photo_id
end

class Parentage < Sequel::Model(:parentage)
  # A parentage is for a given cultivar; there can be multiple parentage records
  # (type: father or mother), or one (type seedling) or none
  #
  many_to_one :cultivar, :class => :Cultivar, :key => :cultivar_id
  many_to_one :parent, :class => :Cultivar, :key => :parent_id
  many_to_one :par_type, :class => :ParentageType, :key => :ptype_id
end

class Photo < Sequel::Model(:photo)
end

### SERVER Part #############################

class ODataFiveApples < Safrano::ServerApp
  publish_service do
    title 'Five apples OData API'
    name 'FiveApplesService'
    namespace 'ODataFiveApples'
    path_prefix '/odata'
    if Safrano::VERSION >= '0.4.3'
      server_url "http://#{FiveA.host}:#{FiveA.port}"
      #       server_url "http://0.0.0.0:#{FiveA.port}"
    end

    enable_batch
    #    bugfix_create_response false
    publish_media_model Photo do
      slug :name
      use Safrano::Media::Static, :root => FiveA.media_root
    end

    publish_model Cultivar, :cultivar do
      add_nav_prop_single :breeder
      add_nav_prop_collection :parent
      add_nav_prop_collection :child
      add_nav_prop_collection :photo
    end
    publish_model Breeder, :breeder do
      add_nav_prop_collection :cultivar
      add_nav_prop_single :avatar
    end
    publish_model ParentageType, :par_type
    publish_model Parentage, :parentage do
      add_nav_prop_single :parent
      add_nav_prop_single :cultivar
      add_nav_prop_single :par_type
    end
  end
end
